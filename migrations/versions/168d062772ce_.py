"""empty message

Revision ID: 168d062772ce
Revises: 7543a3b1edaa
Create Date: 2021-04-25 08:26:29.913470

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '168d062772ce'
down_revision = '7543a3b1edaa'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('aktivitas',
    sa.Column('aktivitas_id', sa.Integer(), nullable=False),
    sa.Column('list_tweet', sa.String(length=225), nullable=False),
    sa.Column('search_user', sa.String(length=50), nullable=False),
    sa.Column('search_tweet', sa.String(length=225), nullable=False),
    sa.Column('post_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['post_id'], ['post.post_id'], ),
    sa.PrimaryKeyConstraint('aktivitas_id')
    )
    op.create_index(op.f('ix_aktivitas_aktivitas_id'), 'aktivitas', ['aktivitas_id'], unique=False)
    op.drop_column('post', 'list_tweet')
    op.drop_column('post', 'search_user')
    op.drop_column('post', 'search_tweet')
    op.create_unique_constraint(None, 'user', ['user_name'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'user', type_='unique')
    op.add_column('post', sa.Column('search_tweet', sa.VARCHAR(length=225), autoincrement=False, nullable=False))
    op.add_column('post', sa.Column('search_user', sa.VARCHAR(length=50), autoincrement=False, nullable=False))
    op.add_column('post', sa.Column('list_tweet', sa.VARCHAR(length=225), autoincrement=False, nullable=False))
    op.drop_index(op.f('ix_aktivitas_aktivitas_id'), table_name='aktivitas')
    op.drop_table('aktivitas')
    # ### end Alembic commands ###
